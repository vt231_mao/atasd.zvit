﻿#include <stdio.h>
#include <Windows.h>
#include <time.h>

struct CurrentTime
{
	unsigned short Hours : 5;
	unsigned short Minutes : 6;
	unsigned short Seconds : 6;
	unsigned short Year : 7;
	unsigned short Month : 4;
	unsigned short MonthDay : 5;
};
int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);

	unsigned short n;
	unsigned short week;
	const char* arr[] = { "Понеділок", "Вівторок", "Середа", "Четвер", "П'ятниця", "Субота", "Неділя" };

	struct CurrentTime now;

	printf("Години: ");
	scanf_s("%hu", &n);
	now.Hours = n;

	printf("Хвилини: ");
	scanf_s("%hu", &n);

	now.Minutes = n;
	printf("Секунди: ");
	scanf_s("%hu", &n);

	now.Seconds = n;
	printf("Номер дня тижня: ");
	scanf_s("%hu", &week);

	printf("День: ");
	scanf_s("%hu", &n);

	now.MonthDay = n;
	printf("Номер місяця: ");
	scanf_s("%hu", &n);

	now.Month = n;
	printf("Рік: ");
	scanf_s("%hu", &n);

	now.Year = n;
	printf("Час: ");

	if (now.Hours < 10) printf("0%d", now.Hours);
	else printf("%d", now.Hours);

	if (now.Minutes < 10) printf(":0%d", now.Minutes);
	else printf(":%d", now.Minutes);

	if (now.Seconds < 10) printf(":0%d", now.Seconds);
	else printf(":%d", now.Seconds);
	printf("\nДата: ");

	if (now.MonthDay < 10) printf("0%d", now.MonthDay);
	else printf("%d", now.MonthDay);

	if (now.Month < 10) printf(".0%d", now.Month);
	else printf(".%d", now.Month);

	if (now.Year < 10) printf(".200%d", now.Year);
	else printf(".20%d", now.Year);

	printf("\nДень тижня: %s", arr[week - 1]);
	printf("\nОбсяг пам'яті: %d", sizeof(now));
	printf("\nОбсяг пам'яті зі стандартною структурою (time.h): %d", sizeof(struct tm));
}

