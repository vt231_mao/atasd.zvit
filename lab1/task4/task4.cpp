﻿#include <stdio.h>
#include <Windows.h>

// Оголошую об'єднання, яке дозволяє представити число з плаваючою комою на бітовому рівні
union
{
	float n;

	// Структура для доступу до окремих бітів числа
	struct
	{
		unsigned int bit1 : 1;
		unsigned int bit2 : 1;
		unsigned int bit3 : 1;
		unsigned int bit4 : 1;
		unsigned int bit5 : 1;
		unsigned int bit6 : 1;
		unsigned int bit7 : 1;
		unsigned int bit8 : 1;
		unsigned int bit9 : 1;
		unsigned int bit10 : 1;
		unsigned int bit11 : 1;
		unsigned int bit12 : 1;
		unsigned int bit13 : 1;
		unsigned int bit14 : 1;
		unsigned int bit15 : 1;
		unsigned int bit16 : 1;
		unsigned int bit17 : 1;
		unsigned int bit18 : 1;
		unsigned int bit19 : 1;
		unsigned int bit20 : 1;
		unsigned int bit21 : 1;
		unsigned int bit22 : 1;
		unsigned int bit23 : 1;
		unsigned int bit24 : 1;
		unsigned int bit25 : 1;
		unsigned int bit26 : 1;
		unsigned int bit27 : 1;
		unsigned int bit28 : 1;
		unsigned int bit29 : 1;
		unsigned int bit30 : 1;
		unsigned int bit31 : 1;
		unsigned int bit32 : 1;
	}bites;

	// Структура для доступу до числа по байтах
	struct
	{
		unsigned char b1;
		unsigned char b2;
		unsigned char b3;
		unsigned char b4;
	}bytes;
}fl;

int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);

	printf("Введіть число: ");
	scanf_s("%f", &fl.n);

	// Виведжу числа у бітовому представленні
	printf("\nПобітовий вивід числа: \n%d%d%d%d %d%d%d%d %d%d%d%d %d%d%d%d %d%d%d%d %d%d%d%d %d%d%d%d %d%d%d%d\n", fl.bites.bit32, fl.bites.bit31, fl.bites.bit30, fl.bites.bit29, fl.bites.bit28, fl.bites.bit27, fl.bites.bit26, fl.bites.bit25, fl.bites.bit24, fl.bites.bit23, fl.bites.bit22, fl.bites.bit21, fl.bites.bit20, fl.bites.bit19, fl.bites.bit18, fl.bites.bit17, fl.bites.bit16, fl.bites.bit15, fl.bites.bit14, fl.bites.bit13, fl.bites.bit12, fl.bites.bit11, fl.bites.bit10, fl.bites.bit9, fl.bites.bit8, fl.bites.bit7, fl.bites.bit6, fl.bites.bit5, fl.bites.bit4, fl.bites.bit3, fl.bites.bit2, fl.bites.bit1);

	// Виведжу числа у вигляді байтів
	printf("\nПобайтовий вивід числа: \n%X %X %X %X\n", fl.bytes.b4, fl.bytes.b3, fl.bytes.b2, fl.bytes.b1);

	// Визначаю знак числа
	if (fl.bites.bit32 == 1) printf("\nЗнак: %d (число від'ємне)", fl.bites.bit32);
	else printf("\nЗнак: %d (число додатнє)\n", fl.bites.bit32);

	// Виведення
	printf("\nХарактеристика:\n%d%d%d%d %d%d%d%d\n ", fl.bites.bit31, fl.bites.bit30, fl.bites.bit29, fl.bites.bit28, fl.bites.bit27, fl.bites.bit26, fl.bites.bit25, fl.bites.bit24);
	printf("\nМантиса: \n%d%d%d%d %d%d%d%d %d%d%d%d %d%d%d%d %d%d%d%d %d%d%d\n ", fl.bites.bit23, fl.bites.bit22, fl.bites.bit21, fl.bites.bit20, fl.bites.bit19, fl.bites.bit18, fl.bites.bit17, fl.bites.bit16, fl.bites.bit15, fl.bites.bit14, fl.bites.bit13, fl.bites.bit12, fl.bites.bit11, fl.bites.bit10, fl.bites.bit9, fl.bites.bit8, fl.bites.bit7, fl.bites.bit6, fl.bites.bit5, fl.bites.bit4, fl.bites.bit3, fl.bites.bit2, fl.bites.bit1);
	printf("\nРозмір структури: %d\n", sizeof(fl));
}
