﻿#include <stdio.h>
#include <Windows.h>

union signedShort
{
	signed short num;// Зберігаю ціле число типу signed short
	struct
	{
		signed short value : 15;// Представляю значення числа з 15 бітів
		signed short sign : 1;// Представляю знак числа з 1 біту
	}
	numeric;
};

int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);

	union signedShort n;

	printf("\n Структури даних та об'єднання");
	printf("\n Введіть число: ");
	scanf_s("%hd", &n.num);//Користувач вводить ціле число, яке зберігається в полі num об'єднання n

	if (n.numeric.sign < 0)
	{
		printf("\n Знак: '-'");
		n.numeric.value *= -1;
		printf("\n Значення: %d", n.numeric.value);
	}

	else if (n.numeric.value == 0)
	{
		printf("\n Знак: число індиферентне");
		printf("\n Значення: %d", n.numeric.value);
	}

	else  printf("\n Знак: '+'\n Значення: %d \n", n.numeric.value);

	printf("\n Побітові логічні операції \n");
	printf("\n Введіть число: ");
	scanf_s("%hd", &n.num);
	signed short m = n.num >> 16;

	if (m)
	{
		printf("\n Знак: '-'");
		n.num *= -1;
		printf("\n Число: %d\n", n.num);
	}

	else if (n.numeric.value == 0)
	{
		printf("\n Знак: число індиферентне");
		printf("\nЗначення: %d", n.numeric.value);
	}
	else
		printf(" \n Знак: '+'\n Число: %d \n", n.num);

	return 0;
}

