﻿#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <time.h>
#include <stdbool.h>
#define MAX_N 100000
typedef int elemtype;
struct elem {
	elemtype* value;
	struct elem* next;
	struct elem* prev;
};
struct myList {
	struct elem* head;
	struct elem* tail;
	size_t size;
};
typedef struct elem cNode;
typedef struct myList cList;
cList* createList(void) {
	cList* list = (cList*)malloc(sizeof(cList));
	if (list) {
		list->size = 0;
		list->head = list->tail = NULL;
	}
	return list;
}
void deleteList(cList* list) {
	cNode* head = list->head;
	cNode* next = NULL;
	while (head) {
		next = head->next;
		free(head);
		head = next;
	}
	free(list);
	list = NULL;
}
bool isEmptyList(cList* list) {
	return ((list->head == NULL) || (list->tail == NULL));
}
int pushBack(cList* list, elemtype* data) {
	cNode* node = (cNode*)malloc(sizeof(cNode));
	if (!node) {
		return -3;
	}
	node->value = data;
	node->next = NULL;
	node->prev = list->tail;
	if (!isEmptyList(list)) {
		list->tail->next = node;
	}
	else {
		list->head = node;
	}
	list->tail = node;
	list->size++;
	return 0;
}
int popBack(cList* list, elemtype** data) {
	cNode* node = NULL;
	if (isEmptyList(list)) {
		return -4;
	}
	node = list->tail;
	list->tail = list->tail->prev;
	if (!isEmptyList(list)) {
		list->tail->next = NULL;
	}
	else {
		list->head = NULL;
	}
	*data = node->value;
	list->size--;
	free(node);
	return 0;
}
void printList(cList* list, void (*func)(elemtype*)) {
	cNode* node = list->head;
	if (isEmptyList(list)) {
		return;
	}
	while (node) {
		func(node->value);
		node = node->next;
	}
}
void printNode(elemtype* value) {
	printf("%d ", *((int*)value));
}
void menu() {
	system("cls");
	printf("1. Сортування вибором (двуспрямований список)\n");
	printf("2. Сортування вставками (масив)\n");
	printf("3. Сортування вставками (двуспрямований список)\n");
	printf("4. Вихід\n");
	printf("Введіть пункт меню: ");
}
int get_variant(int count) {
	int variant;
	scanf_s("%d", &variant);
	return variant;
}
int main() {
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	cList* mylist = createList();
	cList* mylist2 = createList();
	int list[MAX_N], arr[MAX_N], a, b, n, c, min;
	int variant, k;
	srand(time(0));
	printf("Введіть діапазон чисел: [a;b];\na = ");
	scanf_s("%d", &a);
	printf("b = ");
	scanf_s("%d", &b);
	printf("Кількість елементів: ");
	scanf_s("%d", &n);
	do {
		menu();
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 14);
		variant = get_variant(5);
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 15);
		switch (variant) {
		case 1: {
			clock_t begin = clock();
			for (int i = 0; i < n; i++) {
				list[i] = a + rand() % (b - a + 1);
				pushBack(mylist, &list[i]);
			}
			printList(mylist, printNode);
			for (int i = 0; i < n - 1; i++) {
				min = i;
				for (int j = i + 1; j < n; j++) {
					if (list[j] < list[min])
						min = j;
				}
				c = list[i];
				list[i] = list[min];
				list[min] = c;
			}
			printf("\n\nВідсортований зв'язний список\n\n");
			printList(mylist, printNode);
			clock_t end = clock();
			double elapsed_ms = (double)(end - begin) * 1000.0 / CLOCKS_PER_SEC;
			printf("\nЧас сортування: %.2f ms\n", elapsed_ms);
		}
			  break;
		case 2: {
			clock_t begin = clock();
			for (int i = 0; i < n; i++) {
				arr[i] = a + rand() % (b - a + 1);
			}
			for (int i = 0; i < n; i++) {
				printf("%4d", arr[i]);
			}
			for (int i = 1; i < n; i++) {
				c = arr[i];
				for (int j = i - 1; j >= 0 && arr[j] > c; j--) {
					arr[j + 1] = arr[j];
					arr[j] = c;
				}
			}
			printf("\n\nВідсортований масив\n\n");
			for (int i = 0; i < n; i++) {
				printf("%4d", arr[i]);
			}
			clock_t end = clock();
			double elapsed_ms = (double)(end - begin) * 1000.0 / CLOCKS_PER_SEC;
			printf("\nЧас сортування: %.2f ms\n", elapsed_ms);
		}
			  break;
		case 3: {
			clock_t begin = clock();
			for (int i = 0; i < n; i++) {
				list[i] = a + rand() % (b - a + 1);
				pushBack(mylist2, &list[i]);
			}
			printList(mylist2, printNode);
			for (int i = 1; i < n; i++) {
				c = list[i];
				for (int j = i - 1; j >= 0 && list[j] > c; j--) {
					list[j + 1] = list[j];
					list[j] = c;
				}
			}
			printf("\n\nВідсортований зв'язний список\n\n");
			printList(mylist2, printNode);
			clock_t end = clock();
			double elapsed_ms = (double)(end - begin) * 1000.0 / CLOCKS_PER_SEC;
			printf("\nЧас сортування: %.2f ms\n", elapsed_ms);
		}
			  break;
		case 4:
			break;
		}
		if (variant != 4) {
			system("pause");
		}
	} while (variant != 4);
	deleteList(mylist);
	deleteList(mylist2);
	return 0;
}
