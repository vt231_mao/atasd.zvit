﻿#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include<Windows.h>

#define A 69069
#define C 1
#define M 4294967296
#define K 10000
#define N 300

int main() {

    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);

    int i, j, k, freq[N] = { 0 };
    double prob[N], mean, variance, std_dev;
    unsigned int x = 1;

    for (i = 0; i < K; i++) {
        x = (A * x + C) % M;
        int value = x % N;
        freq[value]++;
    }

    mean = 0.0;
    for (j = 0; j < N; j++) {
        prob[j] = (double)freq[j] / K;
        mean += prob[j] * j;
    }

    variance = 0.0;
    for (k = 0; k < N; k++) {
        variance += prob[k] * pow(k - mean, 2);
    }
    std_dev = sqrt(variance);

    printf("Інтервали частот\tСтатистичні ймовірності\n");
    for (i = 0; i < N; i++) {
        printf("%d\t\t\t%.6f\n", freq[i], prob[i]);
    }
    printf("\nСереднє: %.6f\n", mean);
    printf("Дисперсія: %.6f\n", variance);
    printf("Стандартне відхилення: %.6f\n", std_dev);

    return 0;
}

