﻿#include <stdio.h>
#include<windows.h>
#include <stdlib.h>
#include <stdbool.h>

typedef int elemtype;
struct elem {
	elemtype* value;
	struct elem* next;
	struct elem* prev;
};
struct myList {
	struct elem* head;
	struct elem* tail;
	size_t size;
};
typedef struct elem cNode;
typedef struct myList cList;

cList* createList(void) {
	cList* list = (cList*)malloc(sizeof(cList));
	if (list) {
		list->size = 0;
		list->head = list->tail = NULL;
	}
	return list;
}

void deleteList(cList* list) {
	cNode* head = list->head;
	cNode* next = NULL;
	while (head) {
		next = head->next;
		free(head);
		head = next;
	}
	free(list);
	list = NULL;
}

bool isEmptyList(cList* list) {
	return ((list->head == NULL) || (list->tail == NULL));
}

int pushFront(cList* list, elemtype* data) {
	cNode* node = (cNode*)malloc(sizeof(cNode));
	if (!node) {
		return(-1);
	}
	node->value = data;
	node->next = list->head;
	node->prev = NULL;
	if (!isEmptyList(list)) {
		list->head->prev = node;
	}
	else {
		list->tail = node;
	}
	list->head = node;
	list->size++;
	return(0);
}

int popFront(cList* list, elemtype* data) {
	cNode* node;
	if (isEmptyList(list)) {
		return(-2);
	}
	node = list->head;
	list->head = list->head->next;
	if (!isEmptyList(list)) {
		list->head->prev = NULL;
	}
	else {
		list->tail = NULL;
	}
	data = node->value;
	list->size--;
	free(node);
	return(0);
}

int pushBack(cList* list, elemtype* data) {
	cNode* node = (cNode*)malloc(sizeof(cNode));
	if (!node) {
		return(-3);
	}
	node->value = data;
	node->next = NULL;
	node->prev = list->tail;
	if (!isEmptyList(list)) {
		list->tail->next = node;
	}
	else {
		list->head = node;
	}
	list->tail = node;
	list->size++;
	return(0);
}


int popBack(cList* list, elemtype* data) {
	cNode* node = NULL;
	if (isEmptyList(list)) {
		return(-4);
	}
	node = list->tail;
	list->tail = list->tail->prev;
	if (!isEmptyList(list)) {
		list->tail->next = NULL;
	}
	else {
		list->head = NULL;
	}
	data = node->value;
	list->size--;
	free(node);
	return(0);
}


cNode* getNode(cList* list, int index) {
	cNode* node = NULL;
	int i;
	if (index >= list->size) {
		return (NULL);
	}
	if (index < list->size / 2) {
		i = 0;
		node = list->head;
		while (node && i < index) {
			node = node->next;
			i++;
		}
	}
	else {
		i = list->size - 1;
		node = list->tail;
		while (node && i > index) {
			node = node->prev;
			i--;
		}
	}
	return node;
}

void printList(cList* list, void (*func)(elemtype*)) {
	cNode* node = list->head;
	if (isEmptyList(list)) {
		return;
	}
	while (node) {
		func(node->value);
		node = node->next;
	}
}

int pushInside(cList* list, elemtype* data, int index) {
	if (index == 0) {
		return pushFront(list, data);
	}
	if (index == list->size) {
		return pushBack(list, data);
	}
	if (index<0 || index>list->size) {
		return -1;
	}
	cNode* next = getNode(list, index - 1)->next;
	cNode* prev = getNode(list, index)->prev;
	cNode* node = (cNode*)malloc(sizeof(cNode));
	node->value = data;
	getNode(list, index - 1)->next = node;
	getNode(list, index)->prev = node;
	node->next = next;
	node->prev = prev;
	list->size++;

	return 0;
}

int popOutside(cList* list, int index) {
	if (index == 0) {
		elemtype* tmp = 0;
		return popFront(list, tmp);
	}
	if (index == list->size - 1) {
		elemtype* tmp = 0;
		return popBack(list, tmp);
	}
	if (index<0 || index>list->size - 1) {
		return -1;
	}
	cNode* next = getNode(list, index)->next;
	cNode* prev = getNode(list, index)->prev;
	free(getNode(list, index));
	getNode(list, index - 1)->next = next;
	getNode(list, index)->prev = prev;
	list->size--;

	return 0;
}

void printNode(elemtype* value) {
	printf("%d\n", *((int*)value));
}

void menu()
{
	system("cls");
	printf(" 1.Додавання елемента на початок списку\n 2.Додавання елемента в кінець\n 3.Додавання елемента в середину списку\n 4.Видалення елемента з початку списку\n 5.Видалення елементу з кінця списку\n 6.Видалення елемента з середини списку\n 7.Вивід заданого елемента списку\n 8.Вивід списку\n 9.Видалення списку\n 10.Вихід ");
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 14);
	printf("\n\n Введіть пункт меню: ");
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 15);

}
int get_variant(int count) {
	int variant;
	scanf_s("%d", &variant);
	return variant;
}
int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	int variant;
	cList* mylist = createList();
	int value;
	int index;
	int i = 0;
	do {
		menu();
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 14);
		variant = get_variant(10);
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 15);
		switch (variant)
		{
		case 1:
			int a[100];
			printf(" \n Введіть елемент: "); 
			scanf_s("%d", &a[i]);
			pushFront(mylist, &a[i]);
			printList(mylist, printNode);
			i++;
			break;
		case 2:
			printf(" \n Введіть елемент: "); 
			scanf_s("%d", &a[i]);
			pushBack(mylist, &a[i]);
			printList(mylist, printNode);
			i++;
			break;
		case 3:
			printf(" \n Введіть елемент: "); 
			scanf_s("%d", &a[i]);
			printf(" \n Введіть місце в списку: ");
			scanf_s("%d", &index);
			pushInside(mylist, &a[i], index);
			printList(mylist, printNode);
			i++;
			break;
		case 4:
			int tmp;
			popFront(mylist, &tmp);
			printList(mylist, printNode);
			break;
		case 5:
			popBack(mylist, &tmp);
			printList(mylist, printNode);
			break;
		case 6:
			printf(" Введіть номер елемета в списку, котрий хочете видалити: ");
			scanf_s("%d", &index);
			popOutside(mylist, index);
			printList(mylist, printNode);
			i++;
			break;
		case 7:
			printf(" Введіть номер елемента, котрий хочете вивести: "); 
			scanf_s("%d", &value);
			printNode(getNode(mylist, value)->value);
			break;
		case 8:
			printList(mylist, printNode);
			break;
		case 9:
			deleteList(mylist);
			break;
		case 10:
			break;
		}
		if (variant != 10)
			system("pause");
	} while (variant != 10);
}
