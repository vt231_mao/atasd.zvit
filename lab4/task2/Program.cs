﻿using System;
using System.Collections.Generic;

namespace Task_3
{
    class Program
    {
        static int Main(string[] args)
        {
            
            Console.OutputEncoding = System.Text.Encoding.Unicode;
            Console.InputEncoding = System.Text.Encoding.Unicode;

            // Створюю стек для зберігання чисел під час обчислення.
            Stack<double> stack = new Stack<double>();

            double temp = 0, check;
            string expr;

            Console.Write("Введіть вираз (польський запис): ");
            expr = Console.ReadLine();

            // Проходимо по кожному символу в введеному виразі.
            for (int i = 0; i < expr.Length; i++)
            {
                // Перевіряємо, чи є символ оператором.
                if (operators(expr[i]))
                {
                    // Якщо символ є оператором, то витягуємо два верхніх елементи зі стеку.
                    double b = stack.Pop();
                    double a = stack.Pop();

                    // Виконуємо операцію відповідно до оператора.
                    switch (expr[i])
                    {
                        case '+':
                            stack.Push(a + b);
                            break;
                        case '-':
                            stack.Push(a - b);
                            break;
                        case '*':
                            stack.Push(a * b);
                            break;
                        case '/':
                            stack.Push(a / b);
                            break;
                        case '^':
                            stack.Push(Math.Pow(a, b));
                            break;
                    }

                    // Якщо не дійшли до кінця рядка, пропускаємо пробіл.
                    if (i != expr.Length - 1)
                        i++;
                }
                // Перевіряємо, чи є підрядок "sqrt".
                else if (expr[i] == 's')
                {
                    // Якщо є, обчислюємо квадратний корінь верхнього елемента зі стеку.
                    sqrt(expr, i);
                    double a = stack.Pop();
                    stack.Push(Math.Sqrt(a));
                    if (i != expr.Length - 1)
                        i += 3; // Пропускаємо "qrt" після "s"
                }
                // Якщо символ не є оператором або функцією sqrt, то це число.
                else
                {
                    string value = null;

                    // Збираємо всі символи, що складають число.
                    while (!space(expr[i]) && (!operators(expr[i]) || !sqrt(expr, i)) && (expr[i] != '(' || expr[i] != ')'))
                    {
                        value += expr[i];
                        i++;
                        if (i == expr.Length) break;
                    }

                    // Перетворюємо зібраний рядок на число і додаємо його в стек.
                    if (double.TryParse(value, out check) == true)
                        stack.Push(double.Parse(value));
                }
            }

            Console.WriteLine($"\nРезультат: {stack.Peek()}");
            return 0;
        }

        // Функція для перевірки, чи є символ оператором.
        public static bool operators(char a)
        {
            string oprs = "+-*/^";
            for (int i = 0; i < oprs.Length; i++)
                if (a == oprs[i])
                    return true;
            return false;
        }

        // Функція для перевірки, чи є підрядок "sqrt".
        public static bool sqrt(string arr, int a)
        {
            if (arr[a + 1] == 'q' && arr[a + 2] == 'r' && arr[a + 3] == 't')
                return true;
            return false;
        }

        // Функція для перевірки, чи є символ пробілом.
        public static bool space(char a)
        {
            if (a == ' ')
                return true;
            return false;
        }
    }
}
