﻿#include <iostream>
#include <stdio.h>
#include <math.h>
#include <windows.h>

double factorial(int f)
{
	if (f == 0)
		return(1);
	return(f * factorial(f - 1));
}

int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);

	printf("Табулювання функцій \n\n");
	printf("f(n) = n\t\t f(n) = log(n)\t\t f(n) = n*log(n)\t\t f(n) = n^2\t\tf(n) = 2^n\t\t f(n) = n!\n\n");

	double y, y1, y2, y3, y4, y5;

	for (int i = 0; i <= 50; i++)
	{
		y = i;
		printf("f(%d) = %g\t\t", i, y);
		y1 = log(i);
		printf("f(%d) = %.2f\t\t", i, y1);
		y2 = i * log(i);
		printf("f(%d) = %4.2f\t\t", i, y2);
		y3 = pow(i, 2);
		printf("f(%d) = %g\t\t", i, y3);
		y4 = pow(2, i);
		printf("f(%d) = %2.f\t\t ", i, y4);
		y5 = factorial(i);
		printf("f(%d) = %2.f\n", i, y5);
	}

}
