﻿#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <windows.h>

// Функція для заповнення масиву випадковими 0 і 1
void Array(int binaryArr[], int size) 
{
    for (int i = 0; i < size; i++) {
        binaryArr[i] = rand() % 2; // Генеруємо випадкові 0 або 1
    }
}

// Функція для знаходження найбільшого значення, яке можна сформувати у масиві
int findMaxNumber(int binaryArr[], int size) 
{
    int countZeros = 0;
    int countOnes = 0;

    // Підраховую кількість 0 і 1 у масиві
    for (int i = 0; i < size; i++) {
        if (binaryArr[i] == 0) {
            countZeros++;
        }
        else {
            countOnes++;
        }
    }

    // Додаю всі 1 до maxNumber
    int maxNumber = 0;
    for (int i = 0; i < countOnes; i++) {
        maxNumber = maxNumber * 10 + 1; 
    }
    for (int i = 0; i < countZeros; i++) {
        maxNumber = maxNumber * 10; 
    }

    return maxNumber;
}

int main() {
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);

    srand(time(NULL));// Генератор випадкових чисел


    int size;
    printf("Введіть розмір масиву (не більше 40): ");
    scanf_s("%d", &size);

    if (size > 40 || size < 1) {
        printf("Розмір масиву має бути в межах від 1 до 40.\n");
        return 1;
    }

    int binaryArr[40];

    clock_t start_time = clock();
    Array(binaryArr, size);
    clock_t array_time = clock();

    int maxNumber = findMaxNumber(binaryArr, size);
    clock_t end_time = clock();

    double array_duration = ((double)(array_time - start_time)) / CLOCKS_PER_SEC;
    double find_duration = ((double)(end_time - array_time)) / CLOCKS_PER_SEC;

    printf("Масив цифр двійкової системи числення: ");
    for (int i = 0; i < size; i++) {
        printf("%d", binaryArr[i]);
    }
    printf("\n");

    printf("Найбільше можливе число: %d\n", maxNumber);
    printf("Час виконання заповнення масиву: %f секунд\n", array_duration);
    printf("Час виконання пошуку найбільшого числа: %f секунд\n", find_duration);

    return 0;
}

