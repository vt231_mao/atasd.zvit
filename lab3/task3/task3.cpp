﻿#include <stdio.h>
#include<Windows.h>
#include <time.h>

//Функція для обчислення числа Фібоначчі за допомогою рекурсії
int fib(int n) {

    if (n == 0 || n == 1) {//Якщо n дорівнює 0 або 1, повертається n
        return n;
    }
    else {
        return fib(n - 1) + fib(n - 2);
    }

}

int main() {
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);

    int n;
    printf("Введіть номер числа Фібоначчі (n): ");
    scanf_s("%d", &n);

    if (n < 0 || n > 40) {//Перевіряю чи знаходиться n в межах від 0 до 40
        printf("Номер числа Фібоначчі має бути в межах від 0 до 40.\n");
        return 1;
    }
    clock_t start_time = clock();  // Початок вимірювання часу
    int result = fib(n);
    clock_t end_time = clock();    // Кінець вимірювання часу

    double time_taken = ((double)(end_time - start_time)) / CLOCKS_PER_SEC;

    printf("Число Фібоначчі для n = %d:\n %d\n", n, result);//Викликаю функцію fib для обчислення числа Фібоначчі і виводжу 
    printf("Час виконання: %f секунд\n", time_taken);
    return 0;
}

