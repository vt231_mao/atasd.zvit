﻿#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>
#include <string.h>
#include <limits.h>
#include <Windows.h>

#define COUNT_SORT_RANGE 51 

// Функція для обміну двох елементів типу float
void swap_float(float* a, float* b) {
    float temp = *a;
    *a = *b;
    *b = temp;
}

// Функція для обміну двох елементів типу short
void swap_short(short* a, short* b) {
    short temp = *a;
    *a = *b;
    *b = temp;
}

// Функція для підтримки пірамідального порядку в масиві типу float
void heapify_float(float arr[], int n, int i) {
    int largest = i;
    int left = 2 * i + 1;
    int right = 2 * i + 2;

    if (left < n && arr[left] > arr[largest])
        largest = left;

    if (right < n && arr[right] > arr[largest])
        largest = right;

    if (largest != i) {
        swap_float(&arr[i], &arr[largest]);
        heapify_float(arr, n, largest);
    }
}

// Функція для сортування пірамідальним методом масиву типу float
void heapSort_float(float arr[], int n) {
    for (int i = n / 2 - 1; i >= 0; i--)
        heapify_float(arr, n, i);

    for (int i = n - 1; i > 0; i--) {
        swap_float(&arr[0], &arr[i]);
        heapify_float(arr, i, 0);
    }
}

// Функція для сортування Шелла масиву типу float
void shellSort_float(float arr[], int n) {
    for (int gap = n / 2; gap > 0; gap /= 2) {
        for (int i = gap; i < n; i++) {
            float temp = arr[i];
            int j;
            for (j = i; j >= gap && arr[j - gap] > temp; j -= gap) {
                arr[j] = arr[j - gap];
            }
            arr[j] = temp;
        }
    }
}

// Функція для сортування підрахунком масиву типу short
void countSort_short(short arr[], int n) {
    short* output = (short*)malloc(n * sizeof(short));
    int count[COUNT_SORT_RANGE] = { 0 };

    for (int i = 0; i < n; ++i)
        ++count[arr[i] + 10];

    for (int i = 1; i < COUNT_SORT_RANGE; ++i)
        count[i] += count[i - 1];

    for (int i = n - 1; i >= 0; i--) {
        output[count[arr[i] + 10] - 1] = arr[i];
        --count[arr[i] + 10];
    }

    for (int i = 0; i < n; ++i)
        arr[i] = output[i];

    free(output);
}

// Функція для виводу масиву типу float
void printArray_float(float arr[], int size) {
    for (int i = 0; i < size; i++)
        printf("%.2f ", arr[i]);
    printf("\n");
}

// Функція для виводу масиву типу short
void printArray_short(short arr[], int size) {
    for (int i = 0; i < size; i++)
        printf("%d ", arr[i]);
    printf("\n");
}

// Функція для виводу меню користувачу
void menu() {
    printf("1. Пірамідальне сортування (float [-200, 10])\n");
    printf("2. Сортування Шелла (float [-10, 100])\n");
    printf("3. Сортування підрахунком (short [-10, 40])\n");
    printf("4. Вихід\n");
    printf("Введіть пункт меню: ");
}

// Функція для отримання вибору користувача з консолі
int get_variant() {
    int variant;
    scanf_s("%d", &variant);
    return variant;
}

// Функція для вимірювання часу сортування масиву типу float та виведення результат
void measure_sorting_time_float(void (*sort_func)(float*, int), float* arr, int size, const char* sort_name) {
    // Створюю копію масиву для сортування
    float* temp_arr = (float*)malloc(size * sizeof(float));
    memcpy(temp_arr, arr, size * sizeof(float));

    // Виводжу початковий масив перед сортуванням
    printf("Масив перед сортуванням (%s, %d елементів):\n", sort_name, size);
    printArray_float(temp_arr, size);

    // Вимірюю час початку сортування
    clock_t start = clock();
    // Застосовую вибрану функцію сортування
    sort_func(temp_arr, size);
    // Вимірюю час закінчення сортування
    clock_t end = clock();

    // Обчислюю час сортування у мілісекундах
    double elapsed_time = ((double)(end - start)) / CLOCKS_PER_SEC * 1000;

    // Виводжу час сортування та відсортований масив
    printf("Час сортування (%s, %d елементів): %.3f ms\n", sort_name, size, elapsed_time);
    printf("Масив після сортування (%s, %d елементів):\n", sort_name, size);
    printArray_float(temp_arr, size);

    // Звільняю пам'ять, виділену для копії масиву
    free(temp_arr);
}

// Функція для вимірювання часу сортування масиву типу short та виведення результату
void measure_sorting_time_short(void (*sort_func)(short*, int), short* arr, int size, const char* sort_name) {
    // Створюю копію масиву для сортування
    short* temp_arr = (short*)malloc(size * sizeof(short));
    memcpy(temp_arr, arr, size * sizeof(short));

    // Виводжу початковий масив перед сортуванням
    printf("Масив перед сортуванням (%s, %d елементів):\n", sort_name, size);
    printArray_short(temp_arr, size);

    // Вимірюю час початку сортування
    clock_t start = clock();
    // Застосовую вибрану функцію сортування
    sort_func(temp_arr, size);
    // Вимірюю час закінчення сортування
    clock_t end = clock();

    // Обчислюю час сортування у мілісекундах
    double elapsed_time = ((double)(end - start)) / CLOCKS_PER_SEC * 1000;

    // Виводжу час сортування та відсортований масив
    printf("Час сортування (%s, %d елементів): %.3f ms\n", sort_name, size, elapsed_time);
    printf("Масив після сортування (%s, %d елементів):\n", sort_name, size);
    printArray_short(temp_arr, size);

    // Звільняю пам'ять, виділену для копії масиву
    free(temp_arr);
}

// Головна функція
int main() {
    // Налаштовую кодову сторінку консолі для виведення українських символів
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);

    // Ініціалізую генератор випадкових чисел
    srand(time(0));

    bool exit = false;

    // Запускаю головний цикл програми
    while (!exit) {
        // Виводжу меню користувачу
        menu();
        // Отримую вибір користувача
        int variant = get_variant();

        // Обробляю вибір користувача
        switch (variant) {
        case 1: {
            // Створюю масив для сортування
            int size;
            printf("Введіть кількість елементів: ");
            scanf_s("%d", &size);
            float* arr = (float*)malloc(size * sizeof(float));
            // Заповнюю масив випадковими значеннями
            for (int j = 0; j < size; j++) {
                arr[j] = -200 + (float)rand() / RAND_MAX * 210;
            }
            // Вимірюю та виводжу час сортування
            measure_sorting_time_float(heapSort_float, arr, size, "Пірамідальне сортування");
            free(arr);
            break;
        }
        case 2: {
            // Створюю масив для сорту
            int size;
            printf("Введіть кількість елементів: ");
            scanf_s("%d", &size);
            float* arr = (float*)malloc(size * sizeof(float));
            // Заповнюю масив випадковими значеннями
            for (int j = 0; j < size; j++) {
                arr[j] = -10 + (float)rand() / RAND_MAX * 110;
            }
            // Вимірюю та виводжу час сортування
            measure_sorting_time_float(shellSort_float, arr, size, "Сортування Шелла");
            free(arr);
            break;
        }
        case 3: {
            // Створюю масив для сортування
            int size;
            printf("Введіть кількість елементів: ");
            scanf_s("%d", &size);
            short* arr = (short*)malloc(size * sizeof(short));
            // Заповнюю масив випадковими значеннями
            for (int j = 0; j < size; j++) {
                arr[j] = -10 + rand() % 51;
            }
            // Вимірюю та виводжу час сортування
            measure_sorting_time_short(countSort_short, arr, size, "Сортування підрахунком");
            free(arr);
            break;
        }
        case 4:
            // Користувач вибрав вихід
            exit = true;
            break;
        default:
            // Користувач ввів неправильний варіант
            printf("Невірний вибір. Спробуйте ще раз.\n");
        }
        // Виводжу порожній рядок перед наступним виконанням меню, якщо користувач не вибрав вихід
        if (!exit)
            printf("\n");
    }

    return 0;
}
